/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file
 *
 *
 *  Interface header for the BMM_150 Module
 * The interface header exports the following features:   bmm_150_init,
 *                                                        bmm_150_deInit
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BMM_150_IH_H_
#define BMM_150_IH_H_

#include "BCDS_Magnetometer.h"

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* public global variable declarations */
extern Magnetometer_XyzData_T getMagDataRaw;
extern Magnetometer_XyzData_T getMagDataUnit;

/* public function prototype declarations */

/**
 * @brief The function initializes BMM150(magnetometer)creates and starts a autoreloaded
 * timer task which gets and prints the Magnetometer lsb and converted data.
 */
extern void bmm_150_init(void);

/**
 *  @brief API to de-initialize the PMD module
 */
extern void bmm_150_deInit(void);


/* inline function definitions */

#endif /* BMM_150_IH_H_ */

/** ************************************************************************* */
