/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 * @brief
 *   This Application is to demonstrate Gyrosensor Advanced APIs.
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "XdkSensorHandle.h"
#include "XDK_Datalogger_ch.h"
#include "XDK_Datalogger_ih.h"
#include "BMG_160_ch.h"
#include "BMG_160_ih.h"

/* system header files */
#include <stdio.h>
#include <BCDS_Basics.h>

/* additional interface header files */
#include "BCDS_BSP_LED.h"
#include "BSP_BoardType.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Gyroscope.h"
#include "BCDS_Retcode.h"

/* local prototypes ********************************************************* */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */
/* variable to store timer handle*/
xTimerHandle PGD_printTimerHandle_gdt;
Gyroscope_XyzData_T getRawData = { INT32_C(0), INT32_C(0), INT32_C(0) };
Gyroscope_XyzData_T getMdegData = { INT32_C(0), INT32_C(0), INT32_C(0) };
Gyroscope_Bandwidth_T bmg160bw = GYROSCOPE_BANDWIDTH_OUT_OF_RANGE;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */
/**
 * @brief The function initializes BMG160 sensor and set the sensor parameter from logger.ini
 */
void bmg_160_init(void)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnVal = RETCODE_OK;
    /*initialize Gyro sensor*/
    returnValue = Gyroscope_init(xdkGyroscope_BMG160_Handle);

    if (RETCODE_OK == returnValue)
    {
        printf("GyroInit Success\n\r");
    }
    else
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }

        printf("GyroInit Failed\n\r");
    }
    if (strcmp(bmg160_bw, "32") == 0)
    {
        bmg160bw = GYROSCOPE_BMG160_BANDWIDTH_32HZ;
    }
    else if (strcmp(bmg160_bw, "64") == 0)
    {
        bmg160bw = GYROSCOPE_BMG160_BANDWIDTH_64HZ;
    }
    else if (strcmp(bmg160_bw, "12") == 0)
    {
        bmg160bw = GYROSCOPE_BMG160_BANDWIDTH_12HZ;
    }
    else if (strcmp(bmg160_bw, "23") == 0)
    {
        bmg160bw = GYROSCOPE_BMG160_BANDWIDTH_23HZ;
    }
    else if (strcmp(bmg160_bw, "47") == 0)
    {
        bmg160bw = GYROSCOPE_BMG160_BANDWIDTH_47HZ;
    }
    else if (strcmp(bmg160_bw, "116") == 0)
    {
        bmg160bw = GYROSCOPE_BMG160_BANDWIDTH_116HZ;
    }
    else if (strcmp(bmg160_bw, "230") == 0)
    {
        bmg160bw = GYROSCOPE_BMG160_BANDWIDTH_230HZ;
    }
    returnValue = Gyroscope_setBandwidth(xdkGyroscope_BMG160_Handle, bmg160bw);
    if ((RETCODE_OK != returnValue)
            || (GYROSCOPE_BANDWIDTH_OUT_OF_RANGE == bmg160bw))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");

        }
    }
}

/**
 * @brief Read data from Gyro sensor
 *
 * @param[in] pxTimer timer handle
 */
void bmg160_getSensorValues(xTimerHandle pxTimer)
{

    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    (void) pxTimer; /* suppressing warning message */

    /* read Raw sensor data */
    returnValue = Gyroscope_readXyzValue(xdkGyroscope_BMG160_Handle, &getRawData);
    if (RETCODE_OK != returnValue)
    {
        printf("GyrosensorReadRaw Failed\n\r");
    }
    returnValue = Gyroscope_readXyzDegreeValue(xdkGyroscope_BMG160_Handle, &getMdegData);
    if (RETCODE_OK != returnValue)
    {
        printf("GyrosensorReadInMilliDeg Failed\n\r");
    }
}

/**
 *  @brief  The function to de-initialize
 *
 */
void bmg_160_deInit(void)
{
    Retcode_T returnValue = RETCODE_FAILURE;
    returnValue = Gyroscope_deInit(xdkGyroscope_BMG160_Handle);
    if (RETCODE_OK == returnValue)
    {
        printf("gyroscopeSensor Deinit Success\n\r");
    }
    else
    {
        printf("gyroscopeSensor Deinit Failed\n\r");
    }
}

/** ************************************************************************* */
