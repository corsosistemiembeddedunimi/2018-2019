/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 *  Interface header for the MAX_44009 module.
 * The interface header exports the following features:   max_44009_init,
 *                                                        max_44009_deInit
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef MAX_44009_IH_H_
#define MAX_44009_IH_H_

#include "BCDS_Lightsensor.h"
/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* public function prototype declarations */

/**
 * @brief The function initializes light sensor and creates, starts timer task in autoreloaded mode
 * every three second which reads and prints the light sensor data
 */
void max_44009_init(void);

/**
 *  @brief  the function to deinitialize
 *
 */
void max_44009_deInit(void);

/* public global variable declarations */
extern uint16_t luxRawData;
extern uint32_t milliLuxData;
/* inline function definitions */

#endif /* MAX_44009_IH_H_ */

/** ************************************************************************* */
