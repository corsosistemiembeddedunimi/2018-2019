/* esperimenti con CharliePlexing (https://en.wikipedia.org/wiki/Charlieplexing)
 *
 * libreria usata: https://github.com/marcuserronius/ChuckPlex
 *
 * BOARD: wemos D1 mini (cambiano solo i D1, D2, D3)
 */

#include "ChuckPlex.h"

int pins[] = {D1,D2,D3};
int nodes = 6;

int n[]= {-1,3,6,1,4,2,5}; // il primo è solo per avere 1-based, ignorato

ChuckPlex plex = ChuckPlex(pins, 3);

void setup() {
    // print the connections to make
    // you should remove this section once you've done your wiring
    Serial.begin(115200);
    delay(1000);
    plex.displayConnections(nodes);
}

void loop() {
    //randomizer();
    cycle();
}

void cycle() {
    // cycle through the nodes
    for(int i=1; i<=nodes; i++) {
#ifdef DIRECT
        plex.enable(i); // turn off other nodes, enable this one
#else
        plex.enable(n[i]);
#endif
        Serial.println(i);
        delay(50);
    }
}

void randomizer() {
    // choose a node at random
    for(int node=1; node<=nodes; node++) {
        // fade on, then off using PWM
        for(int i=1; i<=16; i++) {
            plex.write(node, i*i-1); // makes a nicer fade than linear
            delay(25);
        }
        for(int i=16; i>=1; i--) {
            plex.write(node, i*i-1);
            delay(25);
        }
    }
}
