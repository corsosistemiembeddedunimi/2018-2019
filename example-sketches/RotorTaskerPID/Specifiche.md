# Esercizio su PID

Dato l'"hardware" mostrato a lezione (ESP32+OLED, fotoresistenza, potenziometro, motore DC, relè, reed) implementare un controllo di velocità PID (Proporzionale, Integrativo, Derivativo) che permetta di regolare la velocità desiderata tramite il potenziometro.
Il rotore deve tentare di rimanere allo stesso regime anche in presenza di carichi variabili.
L'uso dell'OLED è facoltativo, idem per il TaskScheduler.

## Costanti utilizzabili

ESP32 pins a cui sono collegate le varie periferiche:

- ROTOR 12 (relè, output digitale)
- PHOTO 15 (input analogico)
- REED 13 (input pullup digitale)
- POTENZIOMETRO 14 (input analogico)

Periodo del relè, il duty cycle dovrà essere frazione di questo:

- DUTY_PERIOD 500

## Librerie, alternative papabili:

- https://github.com/DrGFreeman/TimedPID
- https://r-downing.github.io/AutoPID/
- https://github.com/mike-matera/FastPID (SCELTA)
