import machine
import time

adc_pin = 36  # Pin per la lettura


def read():
	trimmer = machine.ADC(machine.Pin(adc_pin))  # Creazione oggetto ADC collegato al pin di lettura
	trimmer.atten(trimmer.ATTN_11DB)  # Configurazione range 0-3.6V
	trimmer.width(machine.ADC.WIDTH_10BIT)  # Configurazione lettura su 10b


	while True:
		value = trimmer.read()  # Lettura valore
		print("Value read:", value)
		time.sleep(0.1)
