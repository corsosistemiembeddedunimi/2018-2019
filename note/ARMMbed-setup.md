# Installing the ARM Mbed evironment on Linux
_Michele Ferrante - last update: 13-03-2020_

* Disclaimer: I will be using Arch Linux for the installation process,
if you are using a different distro please refer to
[this guide](https://wiki.archlinux.org/index.php/Installation_guide) first.
_Just kidding_, use whatever you prefer. Just keep in mind that some commands may be different.

* Disclaimer #2: the [Mbed online IDE](https://ide.mbed.com) is an alternative that allows to do everything the mbed-cli does, although I see it as a less practical option.

### Base packages
Mbed CLI is a Python script with "versioning functionalities".

Python 3.6.5+ or 2.7.11 will be needed
(although Python 3 users will be able to use up to Mbed OS 5.9 and CLI Toolchain 1.7.2),
along with the [pip](https://www.pypi.org) package manager.

Versioning will be provided by [Git](https://www.git-scm.com) and [Mercurial](https://www.mercurial-scm.org).
The aforementioned versioning functionalities refer to the fact that every new project is automatically initialized as
a repo and gets the files needed to run pulled from the official [Mbed OS](https://github.com/ARMmbed/mbed-os) library.

The following command will install all the necessary packages using pacman:
```
$ sudo pacman -S python2 python2-pip git mercurial
```
Then install through pip the CLI, either with `sudo` for every user or with the `--user` parameter for the current user only:
```
$ sudo pip2 install mbed-cli
```
> #### Sidenote on --user
> Installing pip packages locally will place them in `~/.local/bin/`.
>
> This folder may or may not be in your PATH wariable.
> If not, add it for the current session with:
> ```
> export PATH=$HOME/.local/bin:$PATH
> ```
> I also recommend to make this change persistent, which can be done (_at least in a barebones OS like Arch_)
> by adding the same export command in the profile file of the preferred shell
> (eg. `.bash_profile` for Bash, `.zprofile` for Zsh).

Now verify the correct installation by running:
```
$ mbed help
```
> The mbed program should automatically run using python2 as per its shebang `#!/usr/bin/python2`.
> If the shebang is `#!/usr/bin/python` instead, check whether you used pip for Python 3.

### Compiler

We need a compiler for the ARM architecture, there are multiple options although many some require a paid license.
I'll be using [GCC ARM](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain)
that is Open Source under the GNU GPL.

For the installation head to the GCC ARM
[release page](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads),
download the latest `gcc-arm-none-eabi-.*-x86_64-linux.tar.bz2` (_there's also the aarch64 Linux version,
for those brave enough to use a Raspberry Pi for more than tinkering and automating stuff, I salute you_).

Go to the download directory and inflate the tarball:
```
$ tar xjf gcc-arm-none-eabi-*-x86_64-linux.tar.bz2
```
And move everything in the desired directory (eg: `/usr/local`).
```
$ sudo cp -a gcc-arm-none-eabi-*-major/* /usr/local
```

> Who makes use of the Arch AUR can install the
> [`gcc-arm-none-eabi-bin`](https://aur.archlinux.org/packages/gcc-arm-none-eabi-bin)
> package that will install the compiler's binary in the `/usr/bin/` directory.
>
> Note that the process above installs both compiler and debugger, meanwhile the AUR installs only the compiler
> because the debugger is installed with
> [`gcc-arm-none-eabi-gdb`](https://www.archlinux.org/packages/community/x86_64/arm-none-eabi-gdb).

### Configuring mbed-cli

Configuration of the tool is done through command line and is stored in multiple locations.
These config files are the global config in `~/.mbed/.mbed` and the local `.mbed` stored in the root directory of your project.
As usual with this kind of setup the local configuration of a varible overrides the value set globally.

The first step of configuration that is **absolutely necessary** is where the Gcc Arm compiler is placed.
```
$ mbed config --global GCC_ARM_PATH "/usr/local/bin"
```
Also, as I have only one compiler installed, I save it as the global `toolchain`:
```
$ mbed config --global toolchain "GCC_ARM"
```

Another option I recommend is `cache`, this helps speeding up some of the processes (eg: `new`) by saving locally small files
often used in projects.
```
$ mbed config --global cache "on"
```

The complete list of options can be found in the
[Mbed OS install guide](https://os.mbed.com/docs/mbed-os/v5.15/tools/configuration-options.html).

_Now that everything is set, it's time to write the "Hello World" of the embedded field:_
```
$ mbed new blink
```
